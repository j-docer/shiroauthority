<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%@page isELIgnored="false" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>main.jsp</title>
</head>
<body>
<h1>欢迎您，<shiro:principal/></h1></br>

<shiro:hasRole name="admin">
    <h1>只有admin角色能够看到的内容</h1><br>
</shiro:hasRole>

<shiro:hasRole name="user">
    <h1>只有user角色能够看到的内容</h1><br>
</shiro:hasRole>

<shiro:hasRole name="user2">
    <h1>只有user2角色能够看到的内容</h1><br>
</shiro:hasRole>

<shiro:hasPermission name="/add">
    <h1>增加用户</h1></br>
    <a href="<%=basePath%>/add">增加学生</a><br>
</shiro:hasPermission>

<shiro:hasPermission name="/delete">
    <h1>删除用户</h1></br>
    <a href="<%=basePath%>/delete">删除学生</a><br>
</shiro:hasPermission>

<shiro:hasPermission name="/update">
    <h1>修改用户</h1></br>
    <a href="<%=basePath%>/update">修改学生</a><br>
</shiro:hasPermission>

<shiro:hasPermission name="/select">
    <h1>查询用户</h1></br>
    <a href="<%=basePath%>/select">查询学生</a><br>
</shiro:hasPermission>

<shiro:authenticated>
<form action="<%=basePath%>/logout" method="post">
    <input type="submit" value="退出"/><br>
    <span style="color: red;">${error}</span>
</form>
</shiro:authenticated>

</body>
</html>
