package study.pj.shiro.authority.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import study.pj.shiro.authority.model.Role2privilege;

import java.util.List;

@Repository
public interface Role2privilegeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role2privilege record);

    int insertSelective(Role2privilege record);

    Role2privilege selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role2privilege record);

    int updateByPrimaryKey(Role2privilege record);

    List<Integer> listPIdByRoleIds(@Param("list")List<Integer> list);
}