package study.pj.shiro.authority.mapper;

import org.springframework.stereotype.Repository;
import study.pj.shiro.authority.model.Role2user;

import java.util.List;

@Repository
public interface Role2userMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role2user record);

    int insertSelective(Role2user record);

    Role2user selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role2user record);

    int updateByPrimaryKey(Role2user record);

    List<Integer> getRoleIdsByUserId(Integer userid);


}