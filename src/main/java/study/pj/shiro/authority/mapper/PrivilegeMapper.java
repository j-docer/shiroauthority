package study.pj.shiro.authority.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import study.pj.shiro.authority.model.Privilege;

import java.util.List;

@Repository
public interface PrivilegeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Privilege record);

    int insertSelective(Privilege record);

    Privilege selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Privilege record);

    int updateByPrimaryKey(Privilege record);

    List<String> getNameByPIdList(@Param("pIdList") List<Integer> pIdList);
}