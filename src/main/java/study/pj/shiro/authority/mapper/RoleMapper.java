package study.pj.shiro.authority.mapper;

import org.springframework.stereotype.Repository;
import study.pj.shiro.authority.model.Role;

@Repository
public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    String getNameById(Integer id);
}