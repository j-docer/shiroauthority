package study.pj.shiro.authority.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import study.pj.shiro.authority.model.Resource;

import java.util.List;

@Repository
public interface ResourceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Resource record);

    int insertSelective(Resource record);

    Resource selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Resource record);

    int updateByPrimaryKey(Resource record);

    List<String> getNameByPIdList(@Param("pidList") List<Integer> pidList);
}