package study.pj.shiro.authority.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CrudTestController {

    @RequiresPermissions("/add")
    @RequestMapping("/add")
    public void addTest() {
        System.out.println("add authority!");
    }

    @RequiresPermissions("/update")
    @RequestMapping("/update")
    public void updateTest() {
        System.out.println("update authority!");
    }

    @RequiresPermissions("/select")
    @RequestMapping("/select")
    public void selectTest() {
        System.out.println("select authority!");
    }

    @RequiresPermissions("/delete")
    @RequestMapping("/delete")
    public void deleteTest() {
        System.out.println("delete authority!");
    }

}
