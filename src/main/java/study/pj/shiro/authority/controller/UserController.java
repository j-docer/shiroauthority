package study.pj.shiro.authority.controller;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import study.pj.shiro.authority.util.PasswordUtil;

@Controller
public class UserController {

//    static Logger logger = LogManager.getLogger(UserController.class);

    @PostMapping(value = "/login")
    public ModelAndView login(String userName, String password) {
//        logger.info("用户登陆{,}", userName, password);

        ModelAndView mav = new ModelAndView();
        //密码加密
        String newPassword = PasswordUtil.encodePwd(password);
        UsernamePasswordToken token = new UsernamePasswordToken(userName, newPassword);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            //mav.addObject("currentUser",userName);
            mav.setViewName("main");
            return mav;
        } catch (Exception e) {
            e.printStackTrace();
            mav.setViewName("index");
            mav.addObject("error", "用户名或密码错误！请检查");
            return mav;
        }
    }

    @RequiresAuthentication
    @RequiresPermissions("/query")
    @RequestMapping(value = "/query")
    public String show() {
        System.out.println("执行查询");
        return "query";
    }
}
