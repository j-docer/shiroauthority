package study.pj.shiro.authority.service;

import study.pj.shiro.authority.model.User;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.Set;

public interface UserService {

    User getUserByName(String name);

    Set<String> findRoles(String userName) throws UserPrincipalNotFoundException;

    Set<String> findPermissions(String userName) throws UserPrincipalNotFoundException;
}
