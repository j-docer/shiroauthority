package study.pj.shiro.authority.service.impl;

import org.springframework.stereotype.Service;
import study.pj.shiro.authority.mapper.*;
import study.pj.shiro.authority.model.User;
import study.pj.shiro.authority.service.UserService;

import javax.annotation.Resource;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

//    @Resource
//    private PrivilegeMapper privilegeMapper;

    @Resource
    private Role2privilegeMapper role2privilegeMapper;

    @Resource
    private ResourceMapper resourceMapper;

    @Resource
    private Role2userMapper role2userMapper;

    @Override
    public User getUserByName(String name) {
        return userMapper.getUserByName(name);
    }

    @Override
    public Set<String> findRoles(String userName) throws UserPrincipalNotFoundException {
        User user = userMapper.getUserByName(userName);
        if (user != null) {
            List<Integer> roleNameList = role2userMapper.getRoleIdsByUserId(user.getId());
            if (roleNameList.size() > 0) {
                Set<String> roleNameSet = new HashSet<>();
                for (Integer roleId : roleNameList) {
                    roleNameSet.add(roleMapper.getNameById(roleId));
                }
                return roleNameSet;
            }
        } else {
            throw new UserPrincipalNotFoundException(userName);
        }
        return null;
    }

    @Override
    public Set<String> findPermissions(String userName) throws UserPrincipalNotFoundException {
        User user = userMapper.getUserByName(userName);
        if (user != null) {
            List<Integer> roleIdsList = role2userMapper.getRoleIdsByUserId(user.getId());
            if (roleIdsList.size() == 0) {
                return null;
            }
            List<Integer> privilegeIdList = role2privilegeMapper.listPIdByRoleIds(roleIdsList);
            List<String> resourceNameList = resourceMapper.getNameByPIdList(privilegeIdList);
            Set<String> result = new HashSet(resourceNameList);
            return result;
        } else {
            throw new UserPrincipalNotFoundException(userName);
        }
    }
}
