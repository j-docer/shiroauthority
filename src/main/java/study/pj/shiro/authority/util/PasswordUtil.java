package study.pj.shiro.authority.util;

import org.apache.shiro.crypto.hash.Md5Hash;
import study.pj.shiro.authority.salt.Salt;

public class PasswordUtil {
    public static String encodePwd(String pwd) {
        return new Md5Hash(pwd, Salt.salt, 2).toString();
    }

     public static void main(String[] args) {
         System.out.println(encodePwd("admin"));
         System.out.println(encodePwd("user"));
         System.out.println(encodePwd("user2"));
    }

}
