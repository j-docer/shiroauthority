package study.pj.shiro.authority.model;

import java.io.Serializable;
import java.util.Date;

/**
 * a_resource
 * @author 
 */
public class Resource implements Serializable {
    private Integer id;

    private String url;

    /**
     * 权限表
     */
    private Integer pid;

    /**
     * 是否为新增的资源 0-是（true） 1- 否(false)
     */
    private Boolean fresh;

    private Date update;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Boolean getFresh() {
        return fresh;
    }

    public void setFresh(Boolean fresh) {
        this.fresh = fresh;
    }

    public Date getUpdate() {
        return update;
    }

    public void setUpdate(Date update) {
        this.update = update;
    }
}