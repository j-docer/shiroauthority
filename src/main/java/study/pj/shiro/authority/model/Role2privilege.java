package study.pj.shiro.authority.model;

import java.io.Serializable;

/**
 * a_role2privilege
 * @author 
 */
public class Role2privilege implements Serializable {
    private Integer id;

    private Integer roleid;

    private Integer pid;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
}