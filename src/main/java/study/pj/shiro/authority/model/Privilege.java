package study.pj.shiro.authority.model;

import java.io.Serializable;
import java.util.Date;

/**
 * a_privilege
 * @author 
 */
public class Privilege implements Serializable {
    private Integer id;

    private String name;

    private Date update;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdate() {
        return update;
    }

    public void setUpdate(Date update) {
        this.update = update;
    }
}