/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : shiro

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-03-15 15:26:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for a_privilege
-- ----------------------------
DROP TABLE IF EXISTS `a_privilege`;
CREATE TABLE `a_privilege` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of a_privilege
-- ----------------------------
INSERT INTO `a_privilege` VALUES ('1', '管理员', '2020-03-14 18:56:32');
INSERT INTO `a_privilege` VALUES ('2', '普通用户', '2020-03-15 13:43:01');

-- ----------------------------
-- Table structure for a_resource
-- ----------------------------
DROP TABLE IF EXISTS `a_resource`;
CREATE TABLE `a_resource` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `url` varchar(150) DEFAULT NULL,
  `pid` int(12) DEFAULT NULL COMMENT '权限表',
  `fresh` tinyint(1) DEFAULT NULL COMMENT '是否为新增的资源 0-是（true） 1- 否(false)',
  `update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='资源访问URL表';

-- ----------------------------
-- Records of a_resource
-- ----------------------------
INSERT INTO `a_resource` VALUES ('1', '/*', '1', '0', '2020-03-14 18:56:48');
INSERT INTO `a_resource` VALUES ('2', '/query', '2', '0', '2020-03-15 13:43:23');
INSERT INTO `a_resource` VALUES ('3', '/add', '2', '0', '2020-03-15 14:04:25');
INSERT INTO `a_resource` VALUES ('4', '/update', '2', '0', '2020-03-15 14:05:04');
INSERT INTO `a_resource` VALUES ('5', '/select', '2', '0', '2020-03-15 14:19:22');
INSERT INTO `a_resource` VALUES ('6', '/delect', '2', '0', '2020-03-15 14:19:33');

-- ----------------------------
-- Table structure for a_role
-- ----------------------------
DROP TABLE IF EXISTS `a_role`;
CREATE TABLE `a_role` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '角色名',
  `update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of a_role
-- ----------------------------
INSERT INTO `a_role` VALUES ('1', 'admin', '2020-03-14 18:56:16');
INSERT INTO `a_role` VALUES ('2', 'user', '2020-03-15 13:42:37');

-- ----------------------------
-- Table structure for a_role2privilege
-- ----------------------------
DROP TABLE IF EXISTS `a_role2privilege`;
CREATE TABLE `a_role2privilege` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `roleid` int(12) DEFAULT NULL,
  `pid` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色权限表';

-- ----------------------------
-- Records of a_role2privilege
-- ----------------------------
INSERT INTO `a_role2privilege` VALUES ('1', '1', '1');
INSERT INTO `a_role2privilege` VALUES ('2', '2', '2');

-- ----------------------------
-- Table structure for a_role2user
-- ----------------------------
DROP TABLE IF EXISTS `a_role2user`;
CREATE TABLE `a_role2user` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `userid` int(12) DEFAULT NULL,
  `roleid` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色权限关系表';

-- ----------------------------
-- Records of a_role2user
-- ----------------------------
INSERT INTO `a_role2user` VALUES ('1', '1', '1');
INSERT INTO `a_role2user` VALUES ('2', '2', '2');

-- ----------------------------
-- Table structure for a_user
-- ----------------------------
DROP TABLE IF EXISTS `a_user`;
CREATE TABLE `a_user` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(250) DEFAULT NULL COMMENT '密码',
  `update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of a_user
-- ----------------------------
INSERT INTO `a_user` VALUES ('1', 'admin', '0130bafca8f0a35681ea25b68eea47fe', '2020-03-14 18:56:05');
INSERT INTO `a_user` VALUES ('2', 'user', 'c8635851eccc03790e326ee48b06cdd4', '2020-03-16 11:54:14');
INSERT INTO `a_user` VALUES ('3', 'user2', 'c8e8b57c32b6a0ed01071e93bb163da6', '2020-03-15 11:54:22');
